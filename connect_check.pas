(*
アルゴリズムとデータ構造
プログラム4.4.4　関節点の検出（２重連結性の判定）改良ver
最終更新日 2016/04/25
質問は j6313099@ed.tus.ac.jp まで
コンパイラ:Free Pascal Compiler(fpc)
*)

(* 使用上の注意 *)
(* このプログラムの本体はbin_connectedになります(本体＝C言語で言うmain関数)。
その中で呼び出しているset_edge関数がグラフの初期化を行っています。そのため、僕が用意しているグラフとは別のものを試したい場合は、set_edge関数内を書き換えるか、新しくset_edge4関数なんかを宣言して呼び出すようにしてください。
なお、デフォルトでは３つのグラフを用意してます。
*)

(* 使用例 *)
(*
 tus> fpc connect_check.pas
 tus> ./connect_check.pas
    > 頂点３は関節点
    > 頂点６は関節点
 tus>
*)

program connect_check;
type 
vindex		       = 1..4; //列挙型
edgelist	       = ^edgecell; 
			    edgecell = record //各頂点
			    destination	: integer;//vindex; //辺の行き先の頂点
			    next	: edgelist; //リストを構成するためのフィールド
			    seq		: integer;
			    state	: boolean; //sequence number 順序数：訪問順のこと
			    searching	: boolean;
			 end;		

var
   vertex  : array[1..9] of
   record adjlist: edgelist;
   end;	   
   once	   : boolean;
   counter : integer;
   state   : array[0..20] of edgecell;
   alfa : array[1..9] of string =
   ('A','B','C','D','E','F','G','H','I');   
procedure set_edge1;
var
   i   : integer;
   //p,q,p2,q2,r2,p3,p4,q4: edgecell;
begin
   (* 1 *)
   state[0].destination := 4;
   state[0].next := nil;
   state[1].destination := 2;
   state[1].next := @state[0];
   vertex[1].adjlist := @state[1];
   (* 2 *)
   state[2].destination := 4;
   state[2].next := nil;
   state[3].destination := 3;
   state[3].next := @state[2];
   state[4].destination := 1;
   state[4].next := @state[3];
   vertex[2].adjlist := @state[4];
   (* 3 *)
   state[5].destination := 2;
   state[5].next := nil;
   vertex[3].adjlist := @state[5];
   (* 4 *)
   state[6].destination := 2;
   state[6].next := nil;
   state[7].destination := 1;
   state[7].next := @state[6];
   vertex[4].adjlist := @state[7];
   
   for i:=1 to 4 do vertex[i].adjlist^.seq := 0;//訪問順を初期化
   once := false;
   counter := 0;
end;

procedure set_edge2;
var
   i   : integer;
   //p,q,p2,q2,r2,p3,p4,q4: edgecell;
begin
   (* 1 *)
   state[0].destination := 4;
   state[0].next := nil;
   state[1].destination := 3;
   state[1].next := @state[0];
   state[2].destination := 2;
   state[2].next := @state[1];
   vertex[1].adjlist := @state[2];
   (* 2 *)
   state[3].destination := 1;
   state[3].next := nil;
   vertex[2].adjlist := @state[3];
   (* 3 *)
   state[4].destination := 1;
   state[4].next := nil;
   state[5].destination := 4;
   state[5].next := @state[4];
   vertex[3].adjlist := @state[5];
   (* 4 *)
   state[6].destination := 1;
   state[6].next := nil;
   state[7].destination := 3;
   state[7].next := @state[6];
   vertex[4].adjlist := @state[7];

   for i:=1 to 4 do vertex[i].adjlist^.seq := 0;//訪問順を初期化
   once := false;
   counter := 0;
end;

procedure set_edge3;
var
   i   : integer;
   //p,q,p2,q2,r2,p3,p4,q4: edgecell;
begin
   (* 1 *)

   state[1].destination := 6;
   state[1].next := nil;
   state[2].destination := 2;
   state[2].next := @state[1];
   vertex[1].adjlist := @state[2];
   (* 2 *)
   state[3].destination := 5;
   state[3].next := nil;
   state[4].destination := 3;
   state[4].next := @state[3];
   state[5].destination := 1;
   state[5].next := @state[4];
   vertex[2].adjlist := @state[5];
   (* 3 *)
   state[6].destination := 5;
   state[6].next := nil;
   state[7].destination := 4;
   state[7].next := @state[6];
   state[8].destination := 2;
   state[8].next := @state[7];
   vertex[3].adjlist := @state[8];
   (* 4 *)
   state[9].destination := 3;
   state[9].next := nil;
   vertex[4].adjlist := @state[9];
   (* 5 *)
   state[10].destination := 6;
   state[10].next := nil;
   state[11].destination := 3;
   state[11].next := @state[10];
   state[12].destination := 2;
   state[12].next := @state[11];
   vertex[5].adjlist := @state[12];
   (* 6 *)
   state[13].destination := 7;
   state[13].next := nil;
   state[14].destination := 5;
   state[14].next := @state[13];
   state[15].destination := 1;
   state[15].next := @state[14];
   vertex[6].adjlist := @state[15];
   (* 7 *)
   state[16].destination := 6;
   state[16].next := nil;
   vertex[7].adjlist := @state[16];
   (* 8 *)

   for i:=1 to 8 do vertex[i].adjlist^.seq := 0;//訪問順を初期化
   once := false;
   counter := 0;
end;


procedure binconnected; // ２重連結性の判定
var
   dummy   : vindex;
   i	     : integer;
   function visit(v:integer) : integer;//vindex;
   var
      min, m : integer;
      p	     : edgelist;
      artic  : boolean;
   begin
      if once then set_edge3();//ここで使用するグラフを切り替える
      counter := counter + 1;
      vertex[v].adjlist^.seq := counter;
      min := counter;
      p := vertex[v].adjlist;
      while p <> nil do
      begin	 
	 if vertex[p^.destination].adjlist^.seq = 0 then 
	 begin	    
	    writeln('v:',alfa[v],' ',alfa[p^.destination],' を訪問する');
	    m := visit(p^.destination);
	    if m < min then min := m;
	    if vertex[v].adjlist^.seq = 1//頂点が根の場合
	       then artic := (vertex[p^.destination].adjlist^.seq <> 2)//子はひとつだけか？
	    else begin artic := (m >= vertex[v].adjlist^.seq);
	       if artic then writeln('v:',alfa[v],' m:',m,' min:',min);end;//根以外
	    if artic then writeln('頂点',alfa[v],'は関節点');
	 end 
	 else if vertex[p^.destination].adjlist^.seq < min then 
	    //ここで探索済みかどうかの判定 = 祖先への逆辺の有無を確認
	    min := vertex[p^.destination].adjlist^.seq;{if終了}
	 p := p^.next;
      end;//while end
      writeln('v:',alfa[v],' ',min, 'を返す');
      visit := min;
   end;//function end

begin // binconnectedの本体
   //多めに見積もってmalloc
   for i:=1 to 9 do new(vertex[i].adjlist);
   counter := 0;//探索順序
   once := true;//一回きりの初期化
   dummy := visit(3);//引数に与えた頂点から探索を始める
   (*セグフォが起こる*)
   //for i:=1 to 9 do dispose(vertex[i].adjlist);
end;

begin//プログラムの本体 手続きbinconnectedを呼び出す
   binconnected();
end.
